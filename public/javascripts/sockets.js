window.addEventListener('load', () => {
    const printMsg = (message) => {
        const newElement = document.createElement('p')
        newElement.innerHTML = message
        document.getElementById("messages").appendChild(newElement)
    }

    const socket = io('ws://localhost:3000', {
        transports: [
            'websocket',
            'flashsocket',
            'htmlfile',
            'jsonp-polling',
        ],
    })

    socket.on('message', data => {
        printMsg(data)
    })
})