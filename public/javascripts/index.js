window.addEventListener('load',  () => {

    const btn = document.getElementsByClassName('submit-btn')[0]

    btn.addEventListener("click", async () => {
        const inputValue = document.getElementsByClassName('text-input')[0].value
        const text = JSON.stringify({message: inputValue})

        const response = await fetch(`http://localhost:3000/`, {
            method: 'POST',
            mode: 'cors',
            credentials: "include",
            body: text,
            headers: {
                'Accept': 'application/json, text/plain, */*',
                'Content-Type': 'application/json'
            }
        })
        console.log('response', await response.json())
    }, false)


})
