const Koa = require('koa')
const render = require('koa-ejs')
const serve = require('koa-static')
const path = require('path')

const router = require('./routes/index')

const app = new Koa()

render(app, {
  root: path.join(__dirname, 'views'),
  layout: false,
  viewExt: 'html',
  cache: false,
  debug: true
});
app.use(serve(__dirname + '/public'))
app.use(router.routes())
app.use(router.allowedMethods())

module.exports = app